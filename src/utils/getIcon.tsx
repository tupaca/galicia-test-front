import React, { ReactElement } from 'react';
import {
    WiDaySunny,
    WiCloudy,
    WiDayCloudy,
    WiDayFog,
    WiDaySnow,
    WiDayRain,
    WiDayStormShowers,
    WiDayShowers,
    WiDaySunnyOvercast,
} from 'react-icons/wi';

export const getIcon = (
    icon = 'soleado',
    color: string,
    size: string,
): ReactElement => {
    switch (icon) {
    case 'Soleado':
        return <WiDaySunny color={color} size={size} />;
    case 'Poco nublado':
        return <WiDaySunnyOvercast color={color} size={size} />;
    case 'Nubes dispersas':
        return <WiDayCloudy color={color} size={size} />;
    case 'Nublado':
        return <WiCloudy color={color} size={size} />;
    case 'Llovizna':
        return <WiDayShowers color={color} size={size} />;
    case 'LLuvia':
        return <WiDayRain color={color} size={size} />;
    case 'Tormenta Electrica':
        return <WiDayStormShowers color={color} size={size} />;
    case 'Nieve':
        return <WiDaySnow color={color} size={size} />;
    case 'Neblina':
        return <WiDayFog color={color} size={size} />;

    default:
        return <WiDaySunny color={color} size={size} />;
    }
};
