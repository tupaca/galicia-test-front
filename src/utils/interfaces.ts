export interface ILoginData {
  user: string;
  pass: string;
}

export interface ISearchWeather {
  city: string;
  country: string;
}

export type Weather = {
  country?: string;
  city?: string;
  day?: string;
  weather?: string;
  temperature?: { celsius: number; fahrenheit: number };
  rainProbability?: number;
  humidity?: number;
  wind?: number;
};
