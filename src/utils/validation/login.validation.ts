import * as yup from 'yup';

const loginValidation = yup
    .object({
        user: yup
            .string()
            .required('El correo es requerido')
            .email('Ingrese un correo válido'),
        pass: yup
            .string()
            .required('La contraseña es requerida')
            .min(8, 'La contraseña debe tener mínimo 8 caracteres'),
    })
    .required();

export default loginValidation;
