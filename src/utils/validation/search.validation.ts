import * as yup from 'yup';

const searchWeatherValidation = yup
    .object({
        country: yup.string().required('El pais es requerido'),
        city: yup.string().required('La ciudad es requerida'),
    })
    .required();

export default searchWeatherValidation;
