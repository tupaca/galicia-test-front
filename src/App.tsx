/* eslint-disable no-use-before-define */
/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useState } from 'react';
import { Footer } from './components/Footer';
import Nav from './components/Nav/Nav';
import { IContext, UserProvider } from './context';
import { Home } from './pages/Home';
import { ILoginData } from './utils/interfaces';

function App() {
    const handleLogin = (data: ILoginData) => {
        setContextData({
            ...contextData,
            userData: {
                user: data.user,
                pass: data.pass,
            },
            isLoggedIn: true,
        });
    };
    const handleLogout = () => {
        setContextData({
            ...contextData,
            userData: {
                user: '',
                pass: '',
            },
            isLoggedIn: false,
        });
    };
    const [contextData, setContextData] = useState<IContext>({
        isLoggedIn: false,
        userData: {},

        handleLogin,
        handleLogout,
    });

    return (
        <UserProvider value={contextData}>
            <div className="App">
                <Nav />
                <div className="layout-body">
                    <Home />
                </div>
                <Footer />
            </div>
        </UserProvider>
    );
}

export default App;
