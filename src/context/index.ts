import React, { useContext } from 'react';
import { ILoginData } from '../utils/interfaces';

export interface IContext {
  userData: {
    user?: string;
    pass?: string;
  };
  isLoggedIn: boolean;
  handleLogin: (data: ILoginData) => void;
  handleLogout: () => void;
}
const userContext = React.createContext<IContext>({
    isLoggedIn: false,
    userData: {},
    handleLogin: () => {},
    handleLogout: () => {},
});

export const UserProvider = userContext.Provider;
export const UserConsumer = userContext.Consumer;
export const useGlobalContext = () => useContext(userContext);
export default userContext;
