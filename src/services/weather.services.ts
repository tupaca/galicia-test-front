import request from '../config/axios';
import { ISearchWeather } from '../utils/interfaces';

export const getWeather = async ({ city, country }: ISearchWeather) => {
    try {
        const response = await request.get('/weather', {
            params: { city, country },
        });

        return response.data.weather;
    } catch (err) {
        return err;
    }
};
