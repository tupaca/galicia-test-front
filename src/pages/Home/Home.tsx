import React, { useState } from 'react';
import Card from '../../components/Card/Card';
import { SearchWeather } from '../../components/SearchWeather';
import Title from '../../components/Title/Title';
import { ViewWeather } from '../../components/ViewWeather';
import { getWeather } from '../../services/weather.services';
import { ISearchWeather, Weather } from '../../utils/interfaces';
import styles from './Home.module.css';

const Home = () => {
    const [weather, setWeather] = useState<Weather | null>(null);
    const [loading, setLoading] = useState(false);
    const handleSearchWeather = async (data: ISearchWeather) => {
        setLoading(true);
        const response = await getWeather(data);
        setWeather(response);
        setLoading(false);
    };
    return (
        <div className={styles.home}>
            <div className={styles.container}>
                <Title size="lg" text="Servicio del clima" upper />
                <div className={styles.bodyCards}>
                    <Card>
                        <SearchWeather handleSearchWeather={handleSearchWeather} />
                    </Card>
                    <Card>
                        <ViewWeather loading={loading} {...weather} />
                    </Card>
                </div>
            </div>
        </div>
    );
};

export default Home;
