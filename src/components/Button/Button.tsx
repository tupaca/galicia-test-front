import React, { FC } from 'react';
import styles from './Button.module.css';

interface IProps {
  text: string | React.ReactElement;
  color: string;
  onClick?: () => void;
  type?: 'button' | 'submit' | 'reset';
}
const Button: FC<IProps> = ({
    text,
    color,
    onClick,
    type = 'button',
}) => (
    <button
        onClick={onClick}
        style={{ backgroundColor: color }}
        className={styles.button}
        type={type}
    >
        {text}
    </button>
);

export default Button;
