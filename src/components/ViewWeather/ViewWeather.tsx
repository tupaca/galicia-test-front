import React, { FC } from 'react';
import { SyncLoader } from 'react-spinners';
import { Title } from '../Title';
import styles from './ViewWeather.module.css';
import { getIcon } from '../../utils/getIcon';
import { Weather } from '../../utils/interfaces';

import { COUNTRIES } from '../../utils/constants';

interface IProps extends Weather {
  loading: boolean;
}
const ViewWeather: FC<IProps | undefined> = ({
    loading,
    city,
    country,
    day,
    humidity,
    rainProbability,
    temperature,
    weather,
    wind,
}) => (
    <div>
        <Title size="md" text="Reporte del Clima" />
        {loading ? (
            <div className={styles.loading}>
                <SyncLoader color="red" loading size={10} />
            </div>
        ) : !city ? (
            <div className={styles.idle}>No hay ningun reporte</div>
        ) : (
            <div className={styles.gridContainer}>
                <div className={styles.gridItem}>
                    <p className={styles.country}>
                        {COUNTRIES.find((state) => state.value === country)?.label}
                    </p>
                    <p className={styles.city}>{city}</p>
                    <p className={styles.day}>{day}</p>
                    <p className={styles.dayWeather}>{weather}</p>
                    <p className={styles.dayCelsiusTemperature}>
                        {temperature?.celsius}
                        <span>
                            <sup> &deg;C</sup>
                        </span>
                    </p>
                    <p className={styles.dayFahrenheitTemperature}>
                        {temperature?.fahrenheit}
                        {' '}
                        &deg;F
                    </p>
                </div>
                <div className={styles.gridItem2}>
                    <div className={styles.iconWeather}>
                        {getIcon(weather, '#c6c6c6', '8em')}
                    </div>
                    <p className={styles.rain}>
                        {' '}
                        Prob. de precipitaciones:
                        {' '}
                        {' '}
                        {rainProbability}
                        %
                    </p>
                    <p className={styles.humidity}>
                        {' '}
                        Humedad:
                        {' '}
                        {humidity}
                        %
                    </p>
                    <p className={styles.wind}>
                        {' '}
                        Viento: a
                        {' '}
                        {wind}
                        {' '}
                        km/h
                    </p>
                </div>
            </div>
        )}
    </div>
);

export default ViewWeather;
