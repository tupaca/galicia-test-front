import { Formik } from 'formik';
import React, { FC } from 'react';
import { ILoginData } from '../../utils/interfaces';
import loginValidation from '../../utils/validation/login.validation';
import { Button } from '../Button';
import { Input } from '../Input';
import styles from './ModalLogin.module.css';

interface IProps {
  show: boolean;
  handleLogin: (data: ILoginData) => void;
}
const ModalLogin: FC<IProps> = ({ show, handleLogin }) => (
    <div style={{ display: show ? 'block' : 'none' }} className={styles.modal}>
        <Formik
            initialValues={{ user: '', pass: '' }}
            validationSchema={loginValidation}
            onSubmit={handleLogin}
        >
            {({
                handleSubmit,
                handleChange,
                handleBlur,
                values,
                errors,
                touched,
            }) => (
                <form onSubmit={handleSubmit}>
                    <div className={styles.input}>
                        <Input
                            label="Usuario"
                            name="user"
                            placeholder="Ingrese su usuario"
                            value={values.user}
                            onChange={handleChange('user')}
                            type="email"
                            onBlur={handleBlur('user')}
                            error={errors.user && touched.user ? errors.user : ''}
                        />
                    </div>
                    <div className={styles.input}>
                        <Input
                            type="password"
                            label="Contraseña"
                            name="pass"
                            placeholder="Ingrese su contraseña"
                            value={values.pass}
                            onChange={handleChange('pass')}
                            error={errors.pass && touched.pass ? errors.pass : ''}
                            onBlur={handleBlur('pass')}
                        />
                    </div>
                    <Button
                        type="submit"
                        onClick={() => {}}
                        color="var(--color-principal)"
                        text="INICIAR SESIÓN"
                    />
                </form>
            )}
        </Formik>
    </div>
);

export default ModalLogin;
