import React, { FC } from 'react';
import styles from './Input.module.css';

interface IProps {
  label: string;
  placeholder: string;
  name: string;
  value: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  type: 'text' | 'password' | 'email';
  error: string | undefined;
  onBlur: (event: React.FocusEvent<HTMLInputElement>) => void;
}
const Input: FC<IProps> = (props) => {
    const { label, error } = props;
    return (
        <>
            <label className={styles.label} htmlFor="pais">
                {label}
            </label>
            <br />
            <input
                className={styles.input}
                {...props}
            />
            <p className={styles.error}>{error}</p>
        </>
    );
};

export default Input;
