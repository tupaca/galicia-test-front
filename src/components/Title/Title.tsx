import React, { FC } from 'react';
import styles from './Title.module.css';

interface IProps {
  text: string;
  size?: 'md' | 'lg';
  color?: string;
  upper?: boolean;
}

const Title: FC<IProps> = ({
    text, size = 'md', color = 'black', upper = false,
}) => (
    <div className={styles.title}>
        <h1 className={`${styles[size]} ${upper ? styles.upper : ''}`} style={{ color }}>
            {text}
        </h1>
    </div>
);

export default Title;
