import { Formik } from 'formik';
import React, { FC } from 'react';
import { ISearchWeather } from '../../utils/interfaces';
import searchWeatherValidation from '../../utils/validation/search.validation';
import { Button } from '../Button';
import { InputSelect } from '../InputSelect';
import { Title } from '../Title';
import styles from './SearchWeather.module.css';
import { CITIES, COUNTRIES } from '../../utils/constants';

interface IProps {
  handleSearchWeather: (data: ISearchWeather) => void;
}
const SearchWeather: FC<IProps> = ({ handleSearchWeather }) => (
    <div>
        <Title size="md" text="Selecciona la Zona" />
        <Formik
            initialValues={{ city: '', country: '' }}
            validationSchema={searchWeatherValidation}
            onSubmit={handleSearchWeather}
        >
            {({
                handleSubmit, values, errors, touched, setFieldValue,
            }) => (
                <form onSubmit={handleSubmit} className={styles.form}>
                    <div className={styles.input}>
                        <InputSelect
                            label="Pais"
                            name="country"
                            onChange={(value: string) => {
                                if (value !== values.country) {
                                    setFieldValue('country', value);
                                    setFieldValue('city', '');
                                }
                            }}
                            options={COUNTRIES}
                            placeholder="Seleccioná un pais"
                            // eslint-disable-next-line max-len
                            value={COUNTRIES.find((country) => country.value === values.country)}
                            errors={errors.country && touched.country ? errors.country : ''}
                        />
                    </div>
                    <div className={styles.input}>
                        <InputSelect
                            label="Ciudad"
                            name="city"
                            onChange={(value: string) => setFieldValue('city', value)}
                            options={CITIES.filter(
                                (city) => city.country === values.country,
                            )}
                            placeholder="Seleccioná una ciudad"
                            // eslint-disable-next-line max-len
                            value={values.city ? CITIES.find((city) => city.value === values.city) : null}
                            errors={errors.city && touched.city ? errors.city : ''}
                        />
                    </div>

                    <div className={styles.button}>
                        <Button
                            type="submit"
                            text="BUSCAR"
                            color="var(--color-principal)"
                        />
                    </div>
                </form>
            )}
        </Formik>
    </div>
);

export default SearchWeather;
