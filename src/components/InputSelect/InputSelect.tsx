import React, { FC } from 'react';
import Select from 'react-select';
import styles from './InputSelect.module.css';

interface IProps {
  label: string;
  placeholder: string;
  name: string;
  onChange: any;
  value: {
    label: string;
    value: string;
    country?: string | undefined;
} | undefined | null;
  options: Array<{ label: string; value: string }>;
  errors: string | undefined;
}
const InputSelect: FC<IProps> = ({
    label,
    name,
    options,
    onChange,
    placeholder,
    errors,
    value,
}) => (
    <>
        <label className={styles.label} htmlFor={name}>
            {label}
        </label>
        <Select
            placeholder={placeholder}
            className={styles.input}
            options={options}
            value={value}
            onChange={(option) => onChange(option?.value)}
            components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
        />
        <p className={styles.error}>{errors}</p>
    </>
);

export default InputSelect;
