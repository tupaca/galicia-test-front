import React, { useState } from 'react';
import { FaUser, FaSignOutAlt } from 'react-icons/fa';
import styles from './Nav.module.css';
import { Button } from '../Button';
import { ModalLogin } from '../ModalLogin';
import { Title } from '../Title';
import { useGlobalContext } from '../../context';

export default function Nav() {
    const [show, setShow] = useState(false);
    const {
        isLoggedIn, userData, handleLogin, handleLogout,
    } = useGlobalContext();

    return (
        <header className={styles.navContainer}>
            <div className={`container ${styles.nav}`}>
                <div className={styles.title}>
                    <Title size="lg" color="white" text="Galicia Seguros" />
                </div>

                {isLoggedIn ? (
                    <div className={styles.user}>
                        {userData.user}
                        <span
                            onClick={() => {
                                setShow(false);
                                handleLogout();
                            }}
                        >
                            <FaSignOutAlt size="1.3em" />
                        </span>
                    </div>
                ) : (
                    <>
                        <div className={styles.button}>
                            <Button
                                text={(
                                    <>
                                        INICIAR SESIÓN
                                        {' '}
                                        <FaUser />
                                    </>
                                )}
                                color="var(--color-principal)"
                                onClick={() => setShow(!show)}
                            />
                        </div>

                        <ModalLogin handleLogin={handleLogin} show={show} />
                    </>
                )}
            </div>
        </header>
    );
}
