import React, { FC } from 'react';
import styles from './Card.module.css';

interface IProps {
  children: React.ReactNode;
}

const Card: FC<IProps> = ({ children }) => (
    <div className={styles.card}>
        <div className={styles.body}>{children}</div>
    </div>
);

export default Card;
