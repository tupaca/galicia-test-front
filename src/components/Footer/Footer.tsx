import React, { FC } from 'react';
import styles from './Footer.module.css';

const Footer: FC = () => (
    <footer className={styles.footerContainer}>
        <div className={`${styles.footer} container`}>
            <p className={styles.copy}>
                Copyright 2022 All rights register |
            </p>
            <p className={styles.company}>Squad Arquitectura Empresarial</p>
        </div>
    </footer>
);

export default Footer;
