# Test Galicia

*Evaluación de conocimientos de Galicia*

# Requisitos Previos
 *Tener instalado Node y NPM*

 *Tener Instalado Yarn*
 
 *Tener instalado Git*
 
###### *Instrucciones ejectuadas en [Docker con WSL](https://docs.docker.com/desktop/windows/wsl/)
# Instalación de Docker y Minikube


- [Instalación de Docker](https://docs.docker.com/desktop/windows/install/)

- *Habilitamos kubernetes en docker*

![Kubernetes](docs/kubernetes.png)

- Instalamos [minikube](https://minikube.sigs.k8s.io/docs/start/)


- Configuramos para que utilice docker

```bash
minikube config set driver docker
minikube delete
```

- Inicializamos el cluster

```bash
minikube start
```

# Deploy

- Clonamos los repositorios

```bash
git clone git@bitbucket.org:tupaca/galicia-test-back.git
git clone git@bitbucket.org:tupaca/galicia-test-front.git
```

- Instalamos las dependencias del proyecto

```bash
cd ./galicia-test-back
yarn install
cd ..

cd ./galicia-test-front
yarn install
yarn build
cd ..
```

- Agregamos el archivo .env en la carpeta galicia-test-back

```
PORT=3000
WEATHER_API_URL=https://api.openweathermap.org/data/2.5
WEATHER_API_KEY= apikey openWeather
```

- Copiamos la carpeta dist a la carpeta public del backend

```bash
cp -r ./galicia-test-front/dist/* ./galicia-test-back/public/
```

- Buildeamos la imagen con docker y la mandamos al cluster

```bash
eval $(minikube docker-env)
docker build -t galicia-app:v1 ./galicia-test-back/.
```

- En la carpeta raiz, a la par de los proyectos clonados ,creamos el archivo de configuración: *deployment.yaml*

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: galicia-app-pod
  labels:
    app: galicia-app
spec:
  containers:
    - name: galicia-app
      image: galicia-app:v1
      imagePullPolicy: Never
      ports:
        - containerPort: 3000

---

apiVersion: v1
kind: Service
metadata:
  name: galicia-app-service 
spec:
  type: NodePort
  ports:
    - port: 3000
      targetPort: 3000
      nodePort: 30010
  selector:
    app: galicia-app
```

- Agregamos un nginx ingress al cluster

```bash
minikube addons enable ingress
```

- Creamos el archivo *ingress.yaml*

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: galicia-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /$1

spec:
  rules:
    - host: localhost
      http:
        paths: 
          - path: /?(.*)
            pathType: Prefix
            backend:
              service:
                name: galicia-app-service
                port:
                  number: 3000
```

- Agregamos estos archivos al cluster con los comandos
    
    ```bash
    kubectl apply -f deployment.yaml
    kubectl apply -f ingress.yaml
    
    ```
    
    ![PodCreated](docs/pod-created.png)
    
    ![IngressCreated](docs/ingress-created.png)
    
- Inicializamos el tunnel de minikube para visualizar la app

```bash
minikube tunnel
```

_En [localhost](http://localhost/ ) estara la aplicación_

## **Construido con**

- [Typescript](https://www.typescriptlang.org/)
- [Express](https://expressjs.com/es/)
- [React](https://es.reactjs.org/)