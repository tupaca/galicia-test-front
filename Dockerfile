# pull official base image
# FROM python:3.7.6-alpine
FROM node:16-alpine as base


# set work directory
WORKDIR /usr/src/galicia-dev








# copy project
COPY . /usr/src/galicia-dev
# install dependencies
RUN yarn install
RUN yarn run build
# run entrypoint.sh

EXPOSE 3000
CMD ["yarn", "dev"]